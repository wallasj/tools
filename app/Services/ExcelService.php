<?php

namespace App\Services;

use App\Video;
use App\Performer;
use App\Model\Cate;
use App\Model\Actor;
use App\Model\Advert;
use App\Model\Videom;
use App\Model\FilmCate;
use App\Model\VideoList;
use App\Model\VideoPerformer;
use Maatwebsite\Excel\Facades\Excel;

class ExcelService {

    public function import(string $path, string $type, string $dbType, string $methods) {
        $cate     = Cate::on($dbType)->pluck('id', 'name')->toArray();
        $filmcate = FilmCate::on($dbType)->pluck('id', 'name')->toArray();
        $advert   = Advert::on($dbType)->pluck('id', 'name')->toArray();
        if ($type == 'video') {
            //$db = $this->_initDB($dbType);
            Excel::import(new Video($dbType, time(), $methods, $cate, $filmcate, $advert), $path);
            //删除文件
            unlink($path);
            return true;
        } else if ($type == 'actor') {
            //$db = $this->_initDB($dbType);
            Excel::import(new Performer($dbType, time(), $methods), $path);
            //删除文件
            unlink($path);
            return true;
        }
        return [];
    }

    private function _initDB($connection)
    {
        return [
            'Videom' => Videom::on($connection),
            'Actor' => Actor::on($connection),
            'VideoList' => VideoList::on($connection),
            'VideoPerformer' => VideoPerformer::on($connection),
            'Advert' => Advert::on($connection)
        ];
    }

}
