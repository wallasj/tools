<?php

namespace App\Console\Commands;

use App\Tools\Helper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class CheckVideos extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:videos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '检查视频资源是否可用';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        Log::info(date('Y-m-d H:i:s') . ' 执行了视频源检测脚本');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $start_time       = microtime(true);
        $process_settings = config('image_download_command');

        $count = 0;
        foreach ($process_settings as $process_setting) {
            $type = $process_setting['type'];
            if ($type != 'video') {
                continue;
            }
            $connection = $process_setting['connection'];
            $table      = $process_setting['table'];
            $join_table = $process_setting['join_table'];

            $query = DB::connection($connection)->table($join_table);
            while (true) {
                $rows = $query->whereNotNull('link')->join($table, $table . '.id', '=', 'video_id')->where($join_table . '.status', 1)->select([
                    $join_table . '.id',
                    $table . '.link',
                ])->orderBy($table . '.id')->limit(100)->get()->toArray();
                $len  = count($rows);
                if (empty($len)) {
                    break;
                }
                $count += $len;
                // 遍历修改状态
                foreach ($rows as $row) {
                    $id           = $row->id;
                    $link         = $row->link;
                    $video_status = $this->_checkVideoExist($id, $link);
                    if ($video_status) {
                        $message = '检查 ' . $id . ' | ' . $link . ' 存在';
                        $this->info($message);
                    } else {
                        // 1.2.4 更新记录
                        $query       = DB::connection($connection)->table($join_table);
                        $save_status = $query->where('id', $id)->update(['status' => 0]);
                        if ($save_status) {
                            $message = '检查 ' . $id . ' | ' . $link . ' 不存在';
                            $this->warn($message);
                        }
                    }
                }
            }

        }

        $end_time       = microtime(true);
        $time_reminding = Helper::formatTiming($end_time - $start_time);

        $message = '共检查了 ' . $count . ' 条视频地址 | 耗时 ' . $time_reminding['hours'] . ':' . $time_reminding['mins'] . ':' . $time_reminding['secs'] . $time_reminding['ms'] . 's';
        $this->info($message);

    }

    /**
     * 检查视频是否存在
     *
     * @param int    $id
     * @param string $uri
     *
     * @return bool
     */
    private function _checkVideoExist(int $id, string $uri) {
        $path_info = parse_url($uri);
        if (empty($path_info['host'])) {
            $message = $id . ' | ' . $uri . ' 视频地址不合法';
            $this->warn($message);
            return false;
        }
        // 获取原始地址
        if (empty($uri)) {
            $message = $id . ' | ' . $uri . ' 视频地址不合法';
            $this->warn($message);
            return false;
        }

        try {
            $header      = get_headers($uri);
            $status_text = $header[0] ?? '';
            $is_exist    = strpos($status_text, '200 OK');
            return $is_exist !== false;
        } catch (\Exception $exception) {
            $message = $id . ' | ' . $uri . ' 视频地址检查出错 ' . $exception->getMessage();
            $this->warn($message);
            return false;
        }
    }
}
