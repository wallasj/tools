<?php

namespace App\Console\Commands;

use App\Tools\Helper;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class DownloadImages extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download:images {--type=a}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '下载图片资源 
                              --type=
                                    a 演员和视频图片
                                    p 演员图片
                                    v 视频图片';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        Log::info(date('Y-m-d H:i:s') . ' 执行了图片下载脚本');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $start_time       = microtime(true);
        $process_settings = config('image_download_command');
        $client           = new Client(['timeout' => 30]);
        $count            = 0;
        // 获取所有的选项值
        $allow_types   = [
            'video',
            'performer',
        ];
        $_process_type = $this->option('type');

        if (empty($_process_type) || $_process_type == 'a') {
            $process_type = $allow_types;
        } else {
            $_process_type = str_replace(['v', 'p'], ['video', 'performer'], $_process_type);
            $process_type  = preg_split('/[,;]/', $_process_type);
        }
        $process_types = array_intersect($allow_types, $process_type);
        if (empty($process_types)) {
            $message = '参数不正确';
            $this->error($message);
        }

        foreach ($process_settings as $process_setting) {
            $connection = $process_setting['connection'];
            $table      = $process_setting['table'];
            $directory  = $process_setting['directory'];
            $type       = $process_setting['type'];
            $join_table = $process_setting['join_table'] ?? '';
            if (!in_array($type, $process_types)) {
                continue;
            }

            // 图片如果为空就置为 0
            $_connection = DB::connection($connection)->table($table);
            $this->_checkImage($_connection);


            while (true) {
                $query = DB::connection($connection)->table($table);
                // 获取 100 条记录
                if ($type == 'performer') {
                    $rows = $query->where('status', 1)->whereNotNull('image')->where('image', '<>', '')->whereNull('local_image_path')->orderBy('id')->limit(100)->get([
                        'id',
                        'image',
                    ])->toArray();
                } else {
                    $rows = $query->whereNotNull('link')->whereNull('local_image_path')->join($join_table, $table . '.id', '=', 'video_id')->where($join_table . '.status', 1)->orderBy($table . '.id')->limit(100)->get([
                        $table . '.id as id',
                        $table . '.image as image',
                    ])->toArray();
                }

                // 计数器
                $len = count($rows);
                if (empty($len)) {
                    break;
                }
                $count += $len;


                // 遍历下载
                foreach ($rows as $row) {
                    $id = $row->id;
                    // 获取原始地址
                    $image_path = $row->image;
                    $image_path = trim($image_path);
                    $file_path  = $this->_processImage($client, $id, $image_path, $directory, $type);

                    // 文件下载失败改成 0
                    if (empty($file_path)) {
                        if ($type == 'video') {
                            $query = DB::connection($connection)->table($join_table);
                            $query->where('video_id', $id)->update(['status' => 0]);
                        } else {
                            $query = DB::connection($connection)->table($table);
                            $query->where('id', $id)->update(['status' => 0]);
                        }
                        continue;
                    }
                    // 更新记录
                    $query       = DB::connection($connection)->table($table);
                    $save_status = $query->where('id', $id)->update(['local_image_path' => $file_path]);
                    if ($save_status) {
                        $message = '下载 ' . $type . ' | ' . $id . ' | ' . $image_path . ' 成功';
                        $this->info($message);
                    }

                }
            }
        }

        $end_time       = microtime(true);
        $time_reminding = Helper::formatTiming($end_time - $start_time);

        $message = '共下载了 ' . $count . ' 张图片 | 耗时 ' . $time_reminding['hours'] . ':' . $time_reminding['mins'] . ':' . $time_reminding['secs'] . $time_reminding['ms'] . 's';
        $this->info($message);
    }

    /**
     * 下载图片
     *
     * @param Client $client
     * @param string $id
     * @param string $uri
     * @param string $directory
     * @param string $type
     *
     * @return bool|string
     */
    private function _processImage(Client $client, string $id, string $uri, string $directory, string $type) {
        $path_info = parse_url($uri);
        if (empty($path_info['host'])) {
            $message = $type . ' | ' . $id . ' | ' . $uri . ' 图片地址不合法';
            $this->warn($message);
            return false;
        }
        // 获取原始地址
        if (empty($uri)) {
            $message = $type . ' | ' . $id . ' | ' . $uri . ' 图片地址不合法';
            $this->warn($message);
            return false;
        }
        // 下载图片
        try {
            $resp          = $client->get($uri);
            $code          = $resp->getStatusCode();
            $content_types = $resp->getHeader('Content-Type');
            $content_type  = array_shift($content_types);
            $content_type  = strtolower($content_type);

            if ($content_type == 'image/jpeg') {
                $ext = 'jpg';
            } else if ($content_type == 'image/png') {
                $ext = 'png';
            } else if ($content_type == 'image/gif') {
                $ext = 'gif';
            } else if ($content_type == 'image/webp') {
                $ext = 'webp';
            } else {
                $message = $type . ' | ' . $id . ' | ' . $uri . ' 图片内容不合法';
                $this->warn($message);
                return false;
            }

            if ($code != 200) {
                $message = $type . ' | ' . $id . ' | ' . $uri . ' 图片下载失败';
                $this->warn($message);
                return false;
            }
            $content = $resp->getBody()->getContents();
        } catch (ClientException $exception) {
            $code = $exception->getCode();
            if ($code == 404) {
                $message = $type . ' | ' . $id . ' | ' . $uri . ' 图片不存在';
                $this->warn($message);
                return false;
            }
            $message = $type . ' | ' . $id . ' | ' . $uri . ' 下载失败' . $exception->getMessage();
            $this->warn($message);
            return false;
        } catch (\Exception $exception) {
            $message = $type . ' | ' . $id . ' | ' . $uri . ' 下载失败' . $exception->getMessage();
            $this->warn($message);
            return false;
        }
        if (empty($content)) {
            $message = $type . ' | ' . $id . ' | ' . $uri . ' 图片获取失败';
            $this->warn($message);
            return false;
        }
        // 保存图片
        // 分目录存放
        $file_paths = [
            $directory,
        ];
        $file_name  = $this->_buildFileName($id, $path_info, $ext);
        array_push($file_paths, $file_name);
        $file_path = implode('/', $file_paths);

        // 判断文件是否存在
        try {
            $storage = Storage::disk('public');
            if ($storage->exists($file_path)) {
                $message = $type . ' | ' . $id . ' | ' . $file_path . ' 图片已存在';
                $this->warn($message);
                return false;
            }
        } catch (\Exception $exception) {
            $message = $type . ' | ' . $id . ' | ' . $file_path . ' 图片检查失败 ' . $exception->getMessage();
            $this->warn($message);
            return false;
        }

        // 保存文件
        try {
            $save_status = Storage::disk('public')->put($file_path, $content);
            if (empty($save_status)) {
                $message = $type . ' | ' . $id . ' | ' . $file_path . ' 图片保存失败';
                $this->warn($message);
                return false;
            }
        } catch (\Exception $exception) {
            $message = $type . ' | ' . $id . ' | ' . $file_path . ' 图片保存失败';
            $this->warn($message);
            return false;
        }
        return $file_path;
    }

    /**
     * 生成文件名
     *
     * @param string $id
     * @param array  $path_info
     * @param string $ext
     *
     * @return string
     */
    private function _buildFileName(string $id, array $path_info, string $ext) {
        $_paths = $path_info['path'];
        $_query = $path_info['query'] ?? '';
        if ($_query) {
            $_paths .= '?' . $_query;
        }
        // 获取目录,路径中带有日期的,如果没有,就放在 sub 中
        $_paths    = str_replace(['/', '-'], '', $_paths);
        $_date_reg = '/\d{4,6}/';
        preg_match($_date_reg, $_paths, $_dates);
        $dir = array_shift($_dates);
        if (empty($dir)) {
            $rand_num = mt_rand(1, 100);
            $subfix   = str_pad($rand_num, 3, '0', STR_PAD_LEFT);
            $dir      = 'sub' . $subfix;
        }

        $file_name = $id . '.' . $ext;
        $file_name = $dir . '/' . $file_name;
        return $file_name;
    }


    /**
     * 将图片地址没有的都统一修改状态为 0
     *
     * @param Builder $connection
     */
    private function _checkImage(Builder $connection) {
        $connection->whereNull('image')->orWhere('image', '')->update([
            'status' => 0,
        ]);
    }
}
