<?php


namespace App\Tools;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Helper {

    /**
     * 对数据进行json编码，不会将中文转为Unicode编码
     *
     * @param mixed $data   需要编码的数据
     * @param int   $option json编码参数，默认忽略Unicode
     *
     * @return false|string
     */
    public static function jsonEncode($data, $option = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) {
        return json_encode($data, $option);
    }

    /**
     * 解析json字符串，忽略Unicode字符.
     *
     * @param string $content json字符串.
     * @param bool   $assoc   是否要转换成数组.
     * @param int    $option  解码参数.
     * @param int    $depth   递归编码深度.
     *
     * @return mixed
     */
    public static function jsonDecode(string $content, $assoc = true, $option = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES, $depth = 512) {
        return json_decode($content, $assoc, $depth, $option);
    }

    /**
     * 将描述格式化成多少年，日，小时，分钟，秒
     *
     * @param float $seconds 时间秒.
     *
     * @return array
     */
    public static function formatTiming(float $seconds): array {
        $year_sec = 3600 * 24 * 365;
        $day_sec  = 3600 * 24;
        $hour_sec = 3600;
        $min_sec  = 60;

        $years = bcdiv($seconds, $year_sec);

        $days = bcdiv(bcmod($seconds, $year_sec), $day_sec);

        $hours = bcdiv(bcmod($seconds, $day_sec), $hour_sec);
        $hours = str_pad($hours, 2, '0', STR_PAD_LEFT);

        $mins = bcdiv(bcmod($seconds, $hour_sec), $min_sec);
        $mins = str_pad($mins, 2, '0', STR_PAD_LEFT);

        $secs = bcmod($seconds, $min_sec);
        $secs = str_pad($secs, 2, '0', STR_PAD_LEFT);

        $mss = bcmod($seconds, 1, 3);
        $mss = str_replace('0.', '.', $mss);

        $data = [
            'years' => $years,
            'days'  => $days,
            'hours' => $hours,
            'mins'  => $mins,
            'secs'  => $secs,
            'ms'    => $mss,
        ];
        return $data;
    }


    /**
     * 记录 SQL 日志
     */
    public static function logSql() {
        DB::listen(function ($query) {
            $bindings = $query->bindings;
            $sql      = $query->sql;
            foreach ($bindings as $replace) {
                $value = is_numeric($replace) ? $replace : "'" . $replace . "'";
                $sql   = preg_replace('/\?/', $value, $sql, 1);
            }
            Log::debug($sql);
        });
    }
}
