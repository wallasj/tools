<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessActorExcel;
use App\Jobs\ProcessVideoExcel;
use App\Model\Actor;
use App\Model\VideoPerformer;
use App\Video;
use App\Performer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends BaseController {
    // 导入视频
    public function index(Request $request) {
        if ($request->isMethod('POST')) {
            $methods = $request->get('methods', 'insert');

            $type = $request->get('dbType', 'yinghua');

            $file_path = $request->get('excel_file'); //获取上传后保存的excel路径
            $real_path = $this->_getRealPath($file_path);

            // 判断文件是否存在
            if ($file_exist = file_exists($real_path)) {
                // 任务队列
                $queue_data = [
                    'excel_file' => $file_path,
                    'type'       => $type,
                    'methods'    => $methods,
                ];
                ProcessVideoExcel::dispatch($queue_data);


                $data = [
                    'code' => 1,
                    'msg'  => '上传成功',
                    'data' => [
                        'src'            => $file_path,
                        'request_params' => $request->post(),
                        'file_info'      => $file_exist,
                    ],
                ];
            } else {
                $data = [
                    'code' => 0,
                    'msg'  => '文件不存在',
                    'data' => [
                        'src'            => $file_path,
                        'request_params' => $request->post(),
                        'file_info'      => $file_exist,
                    ],
                ];
            }
            return $data;
            Excel::import(new Video($type, time(), $methods), $file, \Maatwebsite\Excel\Excel::CSV);

            //return redirect()->route('home.index');
        }
        return view('video');
    }

    /**
     * 上传Excel文件
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function uploadExcel(Request $request) {
        $file = $request->file('excel');               //获取UploadFile实例
        $type = $request->post('type', 'video');

        if (empty($file->isValid())) { //判断文件是否有效
            $data = [
                'code' => 0,
                'msg'  => '文件上传失败,请重新上传',
                'data' => [
                    'src'            => null,
                    'request_params' => $request->post(),
                ],
            ];
            return Response::json($data);
        }

        // 获取文件相关信息
        $ext = $file->getClientOriginalExtension();    // 扩展名

        //文件格式
        $fileTypes = ['xls', 'xlsx', 'csv'];

        //文件格式是否成功
        if (!in_array($ext, $fileTypes)) {
            $data = [
                'code' => 0,
                'msg'  => '上传失败',
                'data' => [
                    'src'            => null,
                    'request_params' => $request->post(),
                ],
            ];
            return Response::json($data);
        }

        $new_file_name = md5_file($file->path()) . '.' . $ext;
        // 将文件移入到指定目录
        $directory = 'unprocessed/' . $type;
        $path      = Storage::disk('excel')->putFileAs($directory, $file, $new_file_name);
        $real_path = $this->_getRealPath($path);


        if ($path) {
            Log::info('文件上传成功,文件路径 => ' . $real_path);
            $data = [
                'code' => 1,
                'msg'  => '上传成功',
                'data' => [
                    'src'            => $path,
                    'request_params' => $request->post(),
                ],
            ];
        } else {
            $data = [
                'code' => 0,
                'msg'  => '上传失败',
                'data' => [
                    'src'            => $path,
                    'request_params' => $request->post(),
                ],
            ];

        }
        return Response::json($data);
    }

    /**
     * 获取文件的绝对路径
     *
     * @param $path
     *
     * @return string
     */
    private function _getRealPath($path) {
        return config('filesystems.disks.excel.root') . '/' . $path;
    }

    public function actor(Request $request) {
        if ($request->isMethod('POST')) {

            $methods = $request->get('methods', 'insert');

            $type = $request->get('dbType', 'yinghua');


            $file_path = $request->get('excel_file'); //获取上传后保存的excel路径
            // 任务队列
            $real_path = $this->_getRealPath($file_path);

            // 判断文件是否存在
            if ($file_exist = file_exists($real_path)) {
                // 任务队列
                $queue_data = [
                    'excel_file' => $file_path,
                    'type'       => $type,
                    'methods'    => $methods,
                ];
                ProcessActorExcel::dispatch($queue_data);

                $data = [
                    'code' => 1,
                    'msg'  => '上传成功',
                    'data' => [
                        'src'            => $file_path,
                        'request_params' => $request->post(),
                        'file_info'      => $file_exist,
                    ],
                ];
            } else {
                $data = [
                    'code' => 0,
                    'msg'  => '文件不存在',
                    'data' => [
                        'src'            => $file_path,
                        'request_params' => $request->post(),
                        'file_info'      => $file_exist,
                    ],
                ];
            }

            return $data;

            Excel::import(new Performer($type, time(), $methods), $file, \Maatwebsite\Excel\Excel::CSV);
            return redirect()->route('home.actor');
        }
        return view('actor');
    }

    public function updatePerformer(Request $request) {
        if ($request->isMethod('POST')) {

            $type     = $request->get('dbType', 'yinghua');
            $data     = [];
            $performe = VideoPerformer::on($type)->get();
            foreach ($performe as $key => $val) {
                if (isset($data[$val->performer_id])) {
                    $data[$val->performer_id] = intval($data[$val->performer_id]) + 1;
                } else {
                    $data[$val->performer_id] = 1;
                }
            }

            if ($data) {
                //先重置
                $result = Actor::on($type)->update([
                    'film_num' => 0,
                ]);
                if ($result !== false) {
                    $success = 0;
                    $failed  = 0;
                    foreach ($data as $key => $val) {
                        $result = Actor::on($type)->where('id', $key)->update([
                            'film_num' => $val,
                        ]);
                        if (!$result) {
                            ++$failed;
                            continue;
                        }
                        ++$success;
                    }

                    return Response::json([
                        'code' => 1,
                        'msg'  => '数据同步成功'
                    ]);
                }
            }

            return Response::json([
                'code' => 0,
                'msg'  => '数据同步失败'
            ]);

        }

        return view('performer');
    }

}
