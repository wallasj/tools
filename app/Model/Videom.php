<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Videom extends Model
{

    public $table = 'video';

    /**
     * Whether the primary key auto-increments.
     *
     * @var bool
     */
    public $incrementing = false;

    const UPDATED_AT = null;

    const CREATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'description',
        'link',
        'image',
        'time',
        'his',
        'format',
        'status',
        'size',
        'record_time',
        'screen_type',
        'type',
        'definition'
    ];

}
