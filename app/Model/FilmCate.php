<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FilmCate extends Model
{

    public $table = 'film_cate';

    /**
     * Whether the primary key auto-increments.
     *
     * @var bool
     */

    const UPDATED_AT = null;

    const CREATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

}
