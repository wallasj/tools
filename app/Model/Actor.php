<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{

    public $table = 'performer';

    /**
     * Whether the primary key auto-increments.
     *
     * @var bool
     */
    public $incrementing = false;

    const UPDATED_AT = null;

    const CREATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'letter',
        'letterall',
        'image',
        'description',
        'status',
        'record_time',
        'film_num',
        'collection_num',
        'sort'
    ];

}
