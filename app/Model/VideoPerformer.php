<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VideoPerformer extends Model
{

    public $table = 'video_performer';

    /**
     * Whether the primary key auto-increments.
     *
     * @var bool
     */

    const UPDATED_AT = null;

    const CREATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'video_id',
        'performer_id',
    ];

}
