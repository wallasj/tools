<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VideoList extends Model
{

    public $table = 'video_list';

    /**
     * Whether the primary key auto-increments.
     *
     * @var bool
     */

    const UPDATED_AT = null;

    const CREATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'video_id',
        'cate',
        'is_free',
        'free_time',
        'unit',
        'diamond',
        'comment_num',
        'collection_num',
        'record_time',
        'status',
        'title',
        'type',
        'description',
        'is_used',
        'used_count',
        'sort',
        'iss_index',
        'advert_id',
        'is_usecomm',
        'banners',
        'is_recommend',
        'video_type',
        'play_num',
        'love',
    ];

}
