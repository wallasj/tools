<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cate extends Model
{

    public $table = 'cate';

    /**
     * Whether the primary key auto-increments.
     *
     * @var bool
     */
    public $incrementing = false;

    const UPDATED_AT = null;

    const CREATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

}
