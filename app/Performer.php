<?php

namespace App;

use App\Model\Actor;
use Maatwebsite\Excel\Row;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class Performer implements OnEachRow, WithCustomCsvSettings {

    private static $connection;

    private static $pinyin;

    private static $time;

    private static $method;

    public function __construct($connection, $time, $method) {
        self::$time = $time;
        self::$method = $method;
        self::$connection = $connection;
        self::$pinyin = app('pinyin');
    }

    public function onRow(Row $row) {
        $rowIndex = $row->getIndex();
        if ($rowIndex > 1) {
            $row   = $row->toArray();
            if (empty($row[2])) return ;

            try {
                $query = Actor::on(self::$connection)->getQuery();
                if (strpos($row[2], '/') !== false) {
                    $names = explode('/', $row[2]);
                    foreach ($names as $val) {
                        $query->orwhere('name', 'like', "%{$val}%");
                    }
                } else {
                    $query->where('name', $row[2]);
                }
                $actors = $query->first();

                if (!$actors && self::$method == 'insert') {
                    $pyString = self::$pinyin->sentence($row[2]);
                    $actors = null;
                    Actor::on(self::$connection)->create([
                        'name'           => $row[2],
                        'letter'         => $pyString ? strtoupper($pyString[0]) : '',
                        'letterall'      => $pyString,
                        'image'          => $row[1],
                        'description'    => $row[0],
                        'status'         => $row[4] == '开启' ? 1 : 0,
                        'record_time'    => self::$time,
                        'film_num'       => 0,
                        'collection_num' => 0,
                        'sort'           => $row[3],
                    ]);
                } else if ($actors || self::$method == 'update') {
                    if ($actors) {
                        $pyString = self::$pinyin->sentence($row[2]);
                        $query->update([
                            'letter'         => $pyString ? strtoupper($pyString[0]) : '',
                            'letterall'      => $pyString,
                            'image'          => $row[1],
                            'description'    => $row[0],
                            'status'         => $row[4] == '开启' ? 1 : 0,
                            'record_time'    => self::$time,
                            'film_num'       => 0,
                            'collection_num' => 0,
                            'sort'           => $row[3]
                        ]);
                        $actors = null;
                    }
                }
            } catch(\Exception $e) {
                Log::info('异常' . $e->getMessage());
            }
        }
    }

    public function getCsvSettings(): array {
        return [
            'input_encoding' => 'GBK',
        ];
    }

}
