<?php

namespace App\Jobs;

use App\Performer;
use App\Video;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class ProcessActorExcel implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;
    public  $timeout = PHP_INT_MAX;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $file_path = $this->data['excel_file'];
        $type      = $this->data['type'];
        $methods   = $this->data['methods'];
        Log::info('$file_path' . $file_path);
        Log::info('$type' . $type);
        Log::info('$methods' . $methods);

        Excel::import(new Performer($type, time(), $methods), $file_path, 'excel');

        Log::info('消费队列' . json_encode($this->data));
    }

}
