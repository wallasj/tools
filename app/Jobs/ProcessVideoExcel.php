<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use App\Video;

class ProcessVideoExcel implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = PHP_INT_MAX;
    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $file_path = $this->data['excel_file'];
        $type      = $this->data['type'];
        $methods   = $this->data['methods'];

        Excel::import(new Video($type, time(), $methods), $file_path, 'excel');
        Log::info('消费队列' . json_encode($this->data));
    }
}
