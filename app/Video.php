<?php

namespace App;

use App\Model\Actor;
use App\Model\VideoList;
use App\Model\Videom;
use App\Model\VideoPerformer;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class Video implements OnEachRow, WithCustomCsvSettings {

    //private $db;

    private static $time;

    private static $cate;

    private static $method;

    private static $advert;

    private static $filmcate;

    private static $connection;

    public function __construct($connection, $time, $method, $cate, $filmcate, $advert) {
        self::$time = $time;
        self::$cate = $cate;
        self::$advert = $advert;
        self::$method = $method;
        self::$filmcate = $filmcate;
        self::$connection = $connection;
    }

    public function onRow(Row $row) {
        $rowIndex = $row->getIndex();
        if ($rowIndex > 1) {
            $row   = $row->toArray();
            if ($row[0]) {
                $simpleRow = [
                    'title' => $row[0],             //标题
                    'link' => $row[1],              //地址
                    'image' => $row[2],             //图片
                    'description' => $row[3],       //描述
                    'number' => $row[4],            //番号
                    'actor' => $row[5],             //演员
                    'videoCate' => $row[6],         //视频分类
                    'movieCate' => $row[7],         //电影分类
                    'is_free' => $row[8],           //是否付费
                    'diamond' => $row[9],           //支付金额
                    'collection_num' => $row[10],   //收藏量
                    'love' => $row[11],             //点赞量
                    'is_used' => $row[12],          //是否使用抵用券
                    'used_count' => $row[13],       //可抵用次数
                    'sort' => $row[14],             //排序
                    'videoAd' => $row[15],          //视频类型
                    'videoBanner' => $row[16],      //视频图片
                    'iss_index' => $row[17],        //是否首页显示
                    'is_recommend' => $row[18],     //是否推荐
                    'status' => $row[19],           //状态
                    'screen_type' => $row[20]       //是否横屏
                ];
                //释放内存
                $row = null;

                //开始执行查询
                Log::info('title - ' . $simpleRow['title']);
                $count = Videom::on(self::$connection)->where('name', $simpleRow['title'])->count();

                //新增
                if ($count <= 0 && self::$method == 'insert') {
                    Log::info('insert');
                    $this->_insert($simpleRow);
                } else if($count > 0 || self::$method == 'update') {
                    Log::info('update');
                    $this->_update($simpleRow);
                }
            }
            //释放内存
            $row = null;
            $simpleRow = null;
        }
    }

    //插入
    private function _insert($simpleRow) {
        $id = uniqid();
        try {
            $video = Videom::on(self::$connection)->create([
                'id'          => $id,
                'name'        => $simpleRow['title'],
                'description' => $simpleRow['description'],
                'link'        => $simpleRow['link'],
                'image'       => $simpleRow['image'],
                'his'         => mt_rand(0, 1) . ':' . date('i:s'),
                'format'      => '',
                'status'      => 1,
                'record_time' => self::$time,
                'screen_type' => $simpleRow['screen_type'] == '横屏' ? 1 : 2,
                'type'        => 1,
                'definition'  => 0,
            ]);

            if (!$video) {
                Log::info('插入视频失败');
                return null;
            }

            $video = null;

            //判断视频类型
            $cateId = 0;
            $videoType = 0;
            if (isset(self::$cate[$simpleRow['videoCate']])) {
                $cateId    = self::$cate[$simpleRow['videoCate']];
                $videoType = 1;
            }

            if (isset(self::$filmcate[$simpleRow['movieCate']])) {
                $cateId    = self::$filmcate[$simpleRow['movieCate']];
                $videoType = 2;
            }

            if ($cateId == 0) {
                $videoType = mt_rand(1, 2);
                $cate      = $videoType == 1 ? self::$cate : self::$filmcate;
                $cateId    = current($cate);
            }

            $result    = VideoList::on(self::$connection)->create([
                'video_id'       => $id,
                'cate'           => $cateId,
                'is_free'        => $simpleRow['is_free'] == '收费' ? 1 : 0,
                'free_time'      => 120,
                'unit'           => 's',
                'diamond'        => floatval($simpleRow['diamond']),
                'comment_num'    => 0,
                'collection_num' => $simpleRow['collection_num'],
                'record_time'    => self::$time,
                'status'         => $simpleRow['status'] == '开启' ? 1 : 0,
                'title'          => $simpleRow['title'],
                'description'    => $simpleRow['description'],
                'is_used'        => $simpleRow['is_used'] == '是' ? 1 : 0,
                'used_count'     => intval($simpleRow['used_count']),
                'sort'           => $simpleRow['sort'],
                'advert_id'      => self::$advert[$simpleRow['videoAd']] ?? 0,
                'iss_index'      => $simpleRow['iss_index'] == '是' ? 1 : 0,
                'is_usecomm'     => 0,
                'banners'        => '[]',
                'is_recommend'   => $simpleRow['is_recommend'] == '是' ? 1 : 0,
                'video_type'     => $videoType,
                'play_num'       => 0,
                'love'           => $simpleRow['love']
            ]);

            if (!$result) {
                Log::info('更新视频列表失败');
                return null;
            }

            $videoId = $result->id;
            $result = null;
            //演员处理
            if (empty($simpleRow['actor'])) return ;

            $actor = str_replace([',', '，'], ',', $simpleRow['actor']);
            $actor  = explode(',', $actor);
            if (empty($actor)) return;

            $insetActor = [];
            foreach ($actor as $val) {
                $ac = Actor::on(self::$connection)->where('name', $val)->select('id')->first();
                if ($ac) {
                    $insetActor[] = [
                        'video_id'     => $videoId,
                        'performer_id' => $ac->id
                    ];
                }
            }

            $return = VideoPerformer::on(self::$connection)->insert($insetActor);
            if (!$return) {
                Log::info('更新视频演员失败');
                return null;
            }
        } catch (\Exception $e) {
            Log::info('插入视频失败' . $e->getMessage());
        }
    }

    //更新
    private function _update($simpleRow) {
        $videoInfo = Videom::on(self::$connection)->where('name', $simpleRow['title'])->first();
        if ($videoInfo) {
            $id = $videoInfo->id;
            try {
                $video = $videoInfo->update([
                    'name'        => $simpleRow['title'],
                    'description' => $simpleRow['description'],
                    'link'        => $simpleRow['link'],
                    'image'       => $simpleRow['image'],
                    'his'         => mt_rand(0, 1) . ':' . date('i:s'),
                    'format'      => '',
                    'status'      => 1,
                    'record_time' => self::$time,
                    'screen_type' => $simpleRow['screen_type'] == '横屏' ? 1 : 2,
                    'type'        => 1,
                    'definition'  => 0,
                ]);
                $videoInfo = null;

                if (!$video) {
                    Log::info('更新视频失败');
                    return null;
                }

                $video = null;

                $VideoList = VideoList::on(self::$connection)->where('video_id', $id)->first();

                if (!$VideoList) {
                    Log::info('视频列表查询失败');
                    return null;
                }

                $videoId = $VideoList->id;

                //判断视频类型
                $cateId = 0;
                $videoType = 0;
                if (isset(self::$cate[$simpleRow['videoCate']])) {
                    $cateId    = self::$cate[$simpleRow['videoCate']];
                    $videoType = 1;
                }

                if (isset(self::$filmcate[$simpleRow['movieCate']])) {
                    $cateId    = self::$filmcate[$simpleRow['movieCate']];
                    $videoType = 2;
                }

                if ($cateId == 0) {
                    $videoType = mt_rand(1, 2);
                    $cate      = $videoType == 1 ? self::$cate : self::$filmcate;
                    $cateId    = current($cate);
                }

                $result    = $VideoList->update([
                    'cate'           => $cateId,
                    'is_free'        => $simpleRow['is_free'] == '收费' ? 1 : 0,
                    'free_time'      => 120,
                    'unit'           => 's',
                    'diamond'        => floatval($simpleRow['diamond']),
                    'comment_num'    => 0,
                    'collection_num' => $simpleRow['collection_num'],
                    'record_time'    => self::$time,
                    'status'         => $simpleRow['status'] == '开启' ? 1 : 0,
                    'title'          => $simpleRow['title'],
                    'description'    => $simpleRow['description'],
                    'is_used'        => $simpleRow['is_used'] == '是' ? 1 : 0,
                    'used_count'     => intval($simpleRow['used_count']),
                    'sort'           => $simpleRow['sort'],
                    'advert_id'      => self::$advert[$simpleRow['videoAd']] ?? 0,
                    'iss_index'      => $simpleRow['iss_index'] == '是' ? 1 : 0,
                    'is_usecomm'     => 0,
                    'banners'        => '[]',
                    'is_recommend'   => $simpleRow['is_recommend'] == '是' ? 1 : 0,
                    'video_type'     => $videoType,
                    'play_num'       => 0,
                    'love'           => $simpleRow['love']
                ]);

                if (!$result) {
                    Log::info('更新视频列表失败');
                    return null;
                }

                $VideoList = null;
                $result = null;
                //演员处理
                if (empty($simpleRow['actor'])) return ;

                $actor = str_replace([',', '，'], ',', $simpleRow['actor']);
                $actor  = explode(',', $actor);
                if (empty($actor)) return;

                $insetActor = [];
                foreach ($actor as $val) {
                    $ac = Actor::on(self::$connection)->where('name', $val)->select('id')->first();
                    if ($ac) {
                        $insetActor[] = [
                            'video_id'     => $videoId,
                            'performer_id' => $ac->id
                        ];
                    }
                }

                if (!empty($insetActor)) {
                    $delete = VideoPerformer::on(self::$connection)->where([
                        'video_id'     => $videoId,
                    ])->delete();

                    if ($delete) {
                        $return = VideoPerformer::on(self::$connection)->insert($insetActor);
                        if (!$return) {
                            Log::info('更新视频演员失败');
                            return null;
                        }
                    }
                }
            } catch (\Exception $e) {
                Log::info('更新视频失败' . $e->getMessage());
            }
        }
    }

    public function getCsvSettings(): array {
        return [
            'input_encoding' => 'GBK',
        ];
    }

}
