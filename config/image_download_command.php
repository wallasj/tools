<?php
return [
    'yinghua_performer'   => [
        'connection' => 'mysql',
        'table'      => 'performer',
        'directory'  => 'performer',
        'type'       => 'performer',
    ],
    'yinghua_video'       => [
        'connection' => 'mysql',
        'table'      => 'video',
        'join_table' => 'video_list',
        'directory'  => 'video',
        'type'       => 'video',
    ],
//    'hanxiucao_performer' => [
//        'connection' => 'hanxiucao_mysql',
//        'table'      => 'performer',
//        'directory'  => 'performer',
//        'type'       => 'performer',
//    ],
//    'hanxiucao_video'     => [
//        'connection' => 'hanxiucao_mysql',
//        'table'      => 'video',
//        'join_table' => 'video_list',
//        'directory'  => 'video',
//        'type'       => 'video',
//    ],
//    'hua_performer'       => [
//        'connection' => 'mysql',
//        'table'      => 'performer',
//        'directory'  => 'performer',
//        'type'       => 'performer',
//    ],
//    'hua_video'           => [
//        'connection' => 'mysql',
//        'table'      => 'video',
//        'join_table' => 'video_list',
//        'directory'  => 'video',
//        'type'       => 'video',
//    ],
];

