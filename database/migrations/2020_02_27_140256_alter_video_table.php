<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterVideoTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('video', function (Blueprint $table) {
            $table->string('local_image_path')->default(null)->nullable(true)->after('image')->comment('本地图片地址');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('video', function (Blueprint $table) {
            $table->dropColumn('local_image_path');
        });
    }
}
