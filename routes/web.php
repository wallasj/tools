<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'upload'], function($router) {
    $router->get('index', 'MethodsController@index')->name('upload.index');
    $router->post('uploads', 'MethodsController@upload')->name('upload.files');
    $router->post('save', 'MethodsController@save')->name('upload.save');
});

Route::group(['prefix' => 'update'], function($router) {
    $router->get('updatePerformer', 'MethodsController@updatePerformer')->name('update.performer');
    $router->post('doUpdatePerformer', 'MethodsController@updatePerformer')->name('update.do');
});

//Route::get('inExcel', 'ExcelController@index')->name('home.index');
//Route::post('upExcel', 'ExcelController@index')->name('home.upexcel');
//Route::post('uploadExcel', 'ExcelController@uploadExcel')->name('home.uploadExcel');
//
//Route::get('inActor', 'ExcelController@actor')->name('home.actor');
//Route::post('upActor', 'ExcelController@actor')->name('home.upActor');
