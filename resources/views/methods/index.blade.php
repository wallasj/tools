<html>
<head>
    <link rel="stylesheet" href="{{ URL::asset('plugins/layui/css/layui.css') }}">
    <script type="text/javascript" src="{{ URL::asset('plugins/layui/layui.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery-1.10.2.min.js') }}"></script>
</head>
<body>
<div class="layui-container">
    <blockquote class="layui-elem-quote">上传文件(新增：请先上传演员，再上传视频！！！)</blockquote>
    <form class="layui-form" method="post" action="" lay-filter="form">
        <div class="layui-form-item">
            <label class="layui-form-label">上传类型</label>
            <div class="layui-input-block">
                <input type="radio" name="type" value="video" title="视频信息" checked="">
                <div class="layui-unselect layui-form-radio layui-form-radioed">
                    <i class="layui-anim layui-icon"></i>
                    <div>视频信息</div>
                </div>
                <input type="radio" name="type" value="actor" title="演员信息">
                <div class="layui-unselect layui-form-radio">
                    <i class="layui-anim layui-icon"></i>
                    <div>演员信息</div>
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">上传方式</label>
            <div class="layui-input-block">
                <input type="radio" name="methods" value="insert" title="新增" checked="">
                <div class="layui-unselect layui-form-radio layui-form-radioed">
                    <i class="layui-anim layui-icon"></i>
                    <div>新增</div>
                </div>
                <input type="radio" name="methods" value="update" title="修改">
                <div class="layui-unselect layui-form-radio">
                    <i class="layui-anim layui-icon"></i>
                    <div>修改</div>
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">数据库</label>
            <div class="layui-input-block">
                <select name="dbType" lay-verify="required">
                    <option value="">请选择</option>
                    @foreach ($databases as $key => $val)
                    <option value="{{$key}}">{{$val}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">文件</label>
            <div class="layui-input-block">
                <div class="layui-hide excel-file-box" style="margin-bottom: 1em">
                    <input type="button" readonly name="excel_file" class="excel-file layui-input" lay-verify="required">
                </div>
                <div class="layui-progress layui-hide upload-box" lay-filter="upload-process">
                    <div class="layui-progress-bar" lay-percent="0%"></div>
                </div>
                <button type="button" class="layui-btn" id="upload">
                    <i class="layui-icon layui-icon-upload"></i>上传
                </button>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="upload-submit" id="submit">开始处理</button>
                <button type="reset" class="layui-btn layui-btn-primary" id="resets">重置</button>
                <a href="/" class="layui-btn layui-btn-primary">返回首页</a>
            </div>
        </div>
    </form>
</div>
<script>
    layui.use(['form', 'upload', 'element', 'layer'], function () {
        var form = layui.form;
        var layer = layui.layer;
        form.on('submit(upload-submit)', function (data) {
            var loadingIndex = layer.load(2, { //icon支持传入0-2
                shade: [0.5, 'gray'], //0.5透明度的灰色背景
                content: '加载中...',
                success: function (layero) {
                    layero.find('.layui-layer-content').css({
                        'padding-top': '39px',
                        'width': '60px'
                    });
                }
            });
            // console.log(data.elem) //被执行事件的元素DOM对象，一般为button对象
            // console.log(data.form) //被执行提交的form对象，一般在存在form标签时才会返回
            // console.log(data.field) //当前容器的全部表单字段，名值对形式：{name: value}
            var field = data.field;
            var uri = "{{route('upload.save')}}";
            $.post(uri, field, function (res) {
                layer.closeAll('loading');
                var msg = res.msg;
                if (res.code) {
                    layer.msg(msg, {icon: 6});
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                } else {
                    layer.msg(msg, {icon: 5});
                }
            });

            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
        });

        var upload = layui.upload;

        var element = layui.element;

        //执行实例
        var uploadInst = upload.render({
            elem: '#upload' //绑定元素
            , url: '{{route('upload.files')}}' //上传接口
            , exts: 'xls|xlsx|csv'
            , accept: 'file'
            , field: 'excel'
            , data: {
                type: function() {
                    return $('form input[name="type"]:checked').val();
                }
            }
            // , auto: false
            , bindAction: '#upload'
            , choose: function () {
                $('.upload-box,.excel-file-box').removeClass('layui-hide');
            }
            , done: function (res) {
                layer.closeAll('loading');
                if (res.code) {
                    var path = res.data.src;
                    $('.excel-file').val(path);
                    layer.msg(res.msg, {icon: 1});
                    return;
                }
                //上传完毕回调
                console.debug(res);
                layer.msg(res.msg, {icon: 5});
            }
            , error: function () {
                //请求异常回调
            }
            , progress: function (n, elem) {
                var percent = n + '%' //获取进度百分比
                element.progress('upload-process', percent); //可配合 layui 进度条元素使用

                var loadingIndex = layer.load(2, { //icon支持传入0-2
                    shade: [0.5, 'gray'], //0.5透明度的灰色背景
                    content: '加载中...',
                    success: function (layero) {
                        layero.find('.layui-layer-content').css({
                            'padding-top': '39px',
                            'width': '60px'
                        });
                    }
                });
                //以下系 layui 2.5.6 新增
                console.log(elem); //得到当前触发的元素 DOM 对象。可通过该元素定义的属性值匹配到对应的进度条。
            }
        });
        //各种基于事件的操作，下面会有进一步介绍
    });
</script>
</body>
</html>
