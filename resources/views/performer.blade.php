<html>
<head>
    <link rel="stylesheet" href="{{ URL::asset('plugins/layui/css/layui.css') }}">
    <script type="text/javascript" src="{{ URL::asset('plugins/layui/layui.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery-1.10.2.min.js') }}"></script>
</head>
<body>
<div class="layui-container">
    <blockquote class="layui-elem-quote">演员影片数量同步</blockquote>
    <form class="layui-form" method="post" action="" lay-filter="form">
        <div class="layui-form-item">
            <label class="layui-form-label">数据库</label>
            <div class="layui-input-block">
                <select name="dbType" lay-verify="required">
                    <option value="">请选择</option>
                    @foreach ($databases as $key => $val)
                        <option value="{{$key}}">{{$val}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="upload-submit" id="submit">一键同步</button>
                <a href="/" class="layui-btn layui-btn-primary">返回首页</a>
            </div>
        </div>
    </form>
</div>
<script>
    layui.use(['form', 'upload', 'element', 'layer'], function () {
        var form = layui.form;
        var layer = layui.layer;

        form.on('submit(upload-submit)', function (data) {
            // console.log(data.elem) //被执行事件的元素DOM对象，一般为button对象
            // console.log(data.form) //被执行提交的form对象，一般在存在form标签时才会返回
            // console.log(data.field) //当前容器的全部表单字段，名值对形式：{name: value}
            var field = data.field;
            var uri = "{{route('update.do')}}";
            var loadingIndex = layer.load(2, { //icon支持传入0-2
                shade: [0.5, 'gray'], //0.5透明度的灰色背景
                content: '加载中...',
                success: function (layero) {
                    layero.find('.layui-layer-content').css({
                        'padding-top': '39px',
                        'width': '60px'
                    });
                }
            });
            $.post(uri, field, function (res) {
                layer.closeAll('loading');
                var msg = res.msg;
                if (res.code) {
                    layer.msg(msg, {icon: 6});
                } else {
                    layer.msg(msg, {icon: 5});
                }
            });

            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
        });
    });
</script>
</body>
</html>
