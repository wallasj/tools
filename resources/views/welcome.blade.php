<html>
<head>
    <link rel="stylesheet" href="{{ URL::asset('plugins/layui/css/layui.css') }}">
    <style>

        .metro-links-nav {
            width: 800px;
            max-width: 100%;
            margin: 10% auto 0;
            overflow: hidden;
        }
        .metro-link {
            position: relative;
            display: block;
            float: left;
            width: 47%;
            margin: 1.5%;
            height: 170px;
            background-color: red;
            box-sizing: border-box;
            transition: all .3s ease;
        }

        .metro-link:hover {
            transform: scale(1.05)
        }

        .mtero-link-txt {
            color: #fff;
            position: absolute;
            top: 20%;
            left: 10%;
            font-size: 20px;
            font-weight: 600;
        }
        @media only screen and (max-width: 1000px) {
            .metro-links-nav {
                margin-top: 20px;
            }
            .metro-link {
                width: 96%;
                margin: 1.5%;
                height: 100px;

            }
            .mtero-link-txt {
                font-size: 16px;
                left: 2.5%;}
        }


    </style>
</head>
<body>
<div class="layui-container metro-links-nav">
    <a href="{{route('upload.index')}}" class="metro-link" style="background: #f37b53;">
        <div class="mtero-link-txt">1、视频/演员导入</div>
    </a>
    <a href="{{route('update.performer')}}" class="metro-link" style="background:#61b9ff;">
        <div class="mtero-link-txt">2、演员数据同步</div>
    </a>
    <a href="#" class="metro-link" style="background:#00a489;">
        <div class="mtero-link-txt">视频资源每天中午12点自动更新</div>
    </a>
    <a href="" class="metro-link" style="background:#847cc5;">
        <div class="mtero-link-txt">图片同步，每天早上8~10点自动更新</div>
    </a>
</div>
</body>
</html>
